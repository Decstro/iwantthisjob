/* eslint-disable linebreak-style */
/* eslint-disable import/prefer-default-export */

export function searchSumOfK(numbers: number[], k: number): Array<number> | false {
  const SEENNUMBERS: any = {};
  for (let i = 0; i < numbers.length; i += 1) {
    const MISSINGNUMBER = k - numbers[i];
    if (SEENNUMBERS[MISSINGNUMBER.toString()]) {
      const SUM_OF_K: Array<any> = [numbers[i], MISSINGNUMBER];
      return SUM_OF_K;
    }
    SEENNUMBERS[numbers[i].toString()] = numbers[i];
  }

  return false;
}
