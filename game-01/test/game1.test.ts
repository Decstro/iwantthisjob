/* eslint-disable linebreak-style */
/* eslint-disable no-undef */
import { searchSumOfK } from '../game1';

const { expect } = require('chai');

describe('Testing the function sumOfK', () => {
  it('The function must return false', () => {
    const EXAMPLE: Array<number> = [14, 90796, 8, 2, 777, 45673,
      29, 203, 456, 32032, 129, 294, 342];
    const N: number = 343;
    const RESULT = searchSumOfK(EXAMPLE, N);
    expect(RESULT).to.equal(false);
  });
  it('The function must return an array with the sum of 141', () => {
    const EXAMPLE: Array<number> = [14, 90796, 8, 2, 777, 45673,
      29, 203, 456, 32032, 129, 294, 342, 1, 12, 15, 23, 34, 70];
    const N: number = 141;
    const RESULT = searchSumOfK(EXAMPLE, N);
    expect(RESULT).to.be.an('array').that.includes(12, 129);
  });
});
