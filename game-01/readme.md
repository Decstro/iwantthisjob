# Game 01 - Sum of K

## Function description

I created a function that return an array of the first possible solution and in case that the solution doesn´t exist return false. When i was doing the code this quote was in my mind: 

>'We're looking to someone who can imagine future problems while is coding'

So I started thinking about a fast and efficient search algorithm that had a good Running time and I thought of binary algorithms, in the use of cache and Hash. Finally i got a simple answer that needs hash, hash table is a good tool for look up values with a good time complexion, the algorithm does a subtraction between the number that you want to search and the actual number and save this value in an array, in other words, saves the missing number of the sum in the hash table, then we have a condition, if the actual number is in the hash, return the value in the position i and the missing number.


### How to use it?
You need to download all the modules so don't forget and put the next command in the terminal, the you can do the test or use the function.

**npm install**

### Testing :clipboard:
I used mocha and chai to test, if you want to test the function just move to the game-01 folder and run this command in the terminal.

**npx mocha test/*.js**

That's all 

 


 
