"use strict";
/* eslint-disable linebreak-style */
/* eslint-disable import/prefer-default-export */
Object.defineProperty(exports, "__esModule", { value: true });
exports.searchSumOfK = void 0;
function searchSumOfK(numbers, k) {
    const SEENNUMBERS = {};
    for (let i = 0; i < numbers.length; i += 1) {
        const MISSINGNUMBER = k - numbers[i];
        if (SEENNUMBERS[MISSINGNUMBER.toString()]) {
            const SUM_OF_K = [numbers[i], MISSINGNUMBER];
            return SUM_OF_K;
        }
        SEENNUMBERS[numbers[i].toString()] = numbers[i];
    }
    return false;
}
exports.searchSumOfK = searchSumOfK;
