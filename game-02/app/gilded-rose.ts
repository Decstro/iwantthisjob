/* eslint-disable linebreak-style */
import {
  verifyGeneralConditions,
  agedBrieItemsUpdate,
  backstagePassesItemsUpdate,
  conjuredItemsUpdate,
  normalItemsUpdate,
} from './updatesfunctions';

export class Item {
  name: string;

  sellIn: number;

  quality: number;

  constructor(name, sellIn, quality) {
    if (new.target === Item) {
      throw new Error('Abstract class cannot be instantiated');
    }

    this.name = name;
    this.sellIn = sellIn;
    this.quality = quality;
  }
}

export class GildedRose extends Item {
  items: Array<Item>;

  constructor(name, sellIn, quality, items = [] as Array<Item>) {
    super(name, sellIn, quality);
    this.items = items;
  }

  staticupdateQuality() {
    for (let i = 0; i < this.items.length; i += 1) {
      if (verifyGeneralConditions(this.items[i])) {
        if (this.items[i].name === 'Aged Brie') {
          agedBrieItemsUpdate(this.items[i]);
        } else if (this.items[i].name === 'Backstage passes to a TAFKAL80ETC concert') {
          backstagePassesItemsUpdate(this.items[i]);
        } else if (this.items[i].name === 'Conjured') {
          conjuredItemsUpdate(this.items[i]);
        } else if (this.items[i].name !== 'Sulfuras, Hand of Ragnaros') {
          normalItemsUpdate(this.items[i]);
        }
      }
    }
    return this.items;
  }
}
