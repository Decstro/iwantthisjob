/* eslint-disable linebreak-style */
/* eslint-disable no-undef */
// eslint-disable-next-line import/no-extraneous-dependencies
import { Item } from '../gilded-rose';
import {
  verifyGeneralConditions,
  agedBrieItemsUpdate,
  backstagePassesItemsUpdate,
  conjuredItemsUpdate,
  normalItemsUpdate,
} from '../updatesfunctions';

const { expect } = require('chai');

describe('Testing the function verifyGeneralConditions', () => {
  it('The function must return the object Item', () => {
    const PRODUCT_AGED: Item = { name: 'Aged Brie', sellIn: 30, quality: 80 };
    expect(verifyGeneralConditions(PRODUCT_AGED)).to.be.an('object');
    expect(verifyGeneralConditions(PRODUCT_AGED)).to.have.property('name');
    expect(verifyGeneralConditions(PRODUCT_AGED)).to.have.property('sellIn');
    expect(verifyGeneralConditions(PRODUCT_AGED)).to.have.property('quality');
  });

  it('The quality value should be 0', () => {
    const PRODUCT: Item = { name: 'Aged Brie', sellIn: 30, quality: -50 };
    verifyGeneralConditions(PRODUCT);
    expect(PRODUCT.quality).to.equal(0);
  });

  it('The item Sulfuras must have a quality equal to 80', () => {
    const PRODUCT: Item = { name: 'Sulfuras, Hand of Ragnaros', sellIn: 30, quality: -50 };
    verifyGeneralConditions(PRODUCT);
    expect(PRODUCT.quality).to.equal(80);
  });

  it('The items quality except for sulfuras must be 0', () => {
    const PRODUCT_AGED: Item = { name: 'Aged Brie', sellIn: 30, quality: 80 };
    const PRODUCT_CONJURED: Item = { name: 'Backstage passes to a TAFKAL80ETC concert', sellIn: 30, quality: 70 };
    const PRODUCT_NORMAL: Item = { name: 'Arroz', sellIn: 30, quality: 65 };
    const PRODUCT_BACKSTAGE: Item = { name: 'Backstage passes to a TAFKAL80ETC concert', sellIn: 30, quality: 90 };
    verifyGeneralConditions(PRODUCT_AGED);
    verifyGeneralConditions(PRODUCT_BACKSTAGE);
    verifyGeneralConditions(PRODUCT_CONJURED);
    verifyGeneralConditions(PRODUCT_NORMAL);
    expect(PRODUCT_AGED.quality).to.equal(0);
    expect(PRODUCT_BACKSTAGE.quality).to.equal(0);
    expect(PRODUCT_CONJURED.quality).to.equal(0);
    expect(PRODUCT_NORMAL.quality).to.equal(0);
  });
});

describe('Testing the function agedBrieItemsUpdate', () => {
  it('The function must return the object Item', () => {
    const PRODUCT_AGED: Item = { name: 'Aged Brie', sellIn: 30, quality: 80 };
    expect(agedBrieItemsUpdate(PRODUCT_AGED)).to.be.an('object');
    expect(agedBrieItemsUpdate(PRODUCT_AGED)).to.have.property('name');
    expect(agedBrieItemsUpdate(PRODUCT_AGED)).to.have.property('sellIn');
    expect(agedBrieItemsUpdate(PRODUCT_AGED)).to.have.property('quality');
  });

  it('The product quality needs to be equal or less than 50', () => {
    const PRODUCT_AGED_NEGATIVE_SELLIN: Item = { name: 'Aged Brie', sellIn: -1, quality: 49 };
    const PRODUCT_AGED_NEGATIVE_SELLIN_Q49: Item = { name: 'Aged Brie', sellIn: -10, quality: 49 };
    const PRODUCT_AGED_POSITIVE_SELLIN: Item = { name: 'Aged Brie', sellIn: 2, quality: 50 };
    agedBrieItemsUpdate(PRODUCT_AGED_NEGATIVE_SELLIN);
    agedBrieItemsUpdate(PRODUCT_AGED_POSITIVE_SELLIN);
    agedBrieItemsUpdate(PRODUCT_AGED_NEGATIVE_SELLIN_Q49);
    expect(PRODUCT_AGED_NEGATIVE_SELLIN.quality).to.be.below(51);
    expect(PRODUCT_AGED_POSITIVE_SELLIN.quality).to.be.below(51);
    expect(PRODUCT_AGED_NEGATIVE_SELLIN_Q49.quality).to.be.below(51);
  });

  it('The product sellin degrades just by one', () => {
    const PRODUCT_AGED_NEGATIVE_SELLIN: Item = { name: 'Aged Brie', sellIn: -1, quality: 50 };
    const PRODUCT_AGED_NEGATIVE_SELLIN_Q49: Item = { name: 'Aged Brie', sellIn: -10, quality: 49 };
    const PRODUCT_AGED_POSITIVE_SELLIN: Item = { name: 'Aged Brie', sellIn: 2, quality: 50 };
    agedBrieItemsUpdate(PRODUCT_AGED_NEGATIVE_SELLIN);
    agedBrieItemsUpdate(PRODUCT_AGED_POSITIVE_SELLIN);
    agedBrieItemsUpdate(PRODUCT_AGED_NEGATIVE_SELLIN_Q49);
    expect(PRODUCT_AGED_NEGATIVE_SELLIN.sellIn).to.equal(-2);
    expect(PRODUCT_AGED_POSITIVE_SELLIN.sellIn).to.equal(1);
    expect(PRODUCT_AGED_NEGATIVE_SELLIN_Q49.sellIn).to.equal(-11);
  });

  it('Quality increases by one when sellin decrease by one', () => {
    const PRODUCT_AGED: Item = { name: 'Aged Brie', sellIn: 2, quality: 32 };
    agedBrieItemsUpdate(PRODUCT_AGED);
    expect(PRODUCT_AGED.quality).to.equal(33);
  });

  it('Quality increases twice as fast when  the sell by date has passed', () => {
    const PRODUCT_AGED1: Item = { name: 'Aged Brie', sellIn: 0, quality: 49 };
    const PRODUCT_AGED2: Item = { name: 'Aged Brie', sellIn: -1, quality: 32 };
    agedBrieItemsUpdate(PRODUCT_AGED1);
    agedBrieItemsUpdate(PRODUCT_AGED2);
    expect(PRODUCT_AGED1.quality).to.equal(50);
    expect(PRODUCT_AGED2.quality).to.equal(34);
  });
});

describe('Testing the function backstagePassesItemsUpdate', () => {
  it('The function must return the object Item', () => {
    const PRODUCT_AGED: Item = { name: 'Aged Brie', sellIn: 30, quality: 80 };
    expect(backstagePassesItemsUpdate(PRODUCT_AGED)).to.be.an('object');
    expect(backstagePassesItemsUpdate(PRODUCT_AGED)).to.have.property('name');
    expect(backstagePassesItemsUpdate(PRODUCT_AGED)).to.have.property('sellIn');
    expect(backstagePassesItemsUpdate(PRODUCT_AGED)).to.have.property('quality');
  });

  it('The product quality drops to 0 after the concert', () => {
    const PRODUCT_BACK_NEGATIVE_SELLIN: Item = { name: 'Backstage passes to a TAFKAL80ETC concert', sellIn: -2, quality: 49 };
    const PRODUCT_BACK_NEGATIVE_SELLIN_S1: Item = { name: 'Backstage passes to a TAFKAL80ETC concert', sellIn: -276, quality: 45 };
    const PRODUCT_BACK_NEGATIVE_SELLIN_Q49: Item = { name: 'Backstage passes to a TAFKAL80ETC concert', sellIn: -10, quality: 50 };
    const PRODUCT_BACK_0: Item = { name: 'Backstage passes to a TAFKAL80ETC concert', sellIn: 0, quality: 8 };
    backstagePassesItemsUpdate(PRODUCT_BACK_0);
    backstagePassesItemsUpdate(PRODUCT_BACK_NEGATIVE_SELLIN);
    backstagePassesItemsUpdate(PRODUCT_BACK_NEGATIVE_SELLIN_S1);
    backstagePassesItemsUpdate(PRODUCT_BACK_NEGATIVE_SELLIN_Q49);
    expect(PRODUCT_BACK_0.quality).to.equal(0);
    expect(PRODUCT_BACK_NEGATIVE_SELLIN.quality).to.equal(0);
    expect(PRODUCT_BACK_NEGATIVE_SELLIN_S1.quality).to.equal(0);
    expect(PRODUCT_BACK_NEGATIVE_SELLIN_Q49.quality).to.equal(0);
  });

  it('The product quality needs to be equal or less than 50', () => {
    const PRODUCT_NEGATIVE_SELLIN: Item = { name: 'Backstage passes to a TAFKAL80ETC concert', sellIn: -1, quality: 49 };
    const PRODUCT_NEGATIVE_SELLIN_Q49: Item = { name: 'Backstage passes to a TAFKAL80ETC concert', sellIn: -10, quality: 49 };
    const PRODUCT_POSITIVE_SELLIN: Item = { name: 'Backstage passes to a TAFKAL80ETC concert', sellIn: 2, quality: 50 };
    agedBrieItemsUpdate(PRODUCT_NEGATIVE_SELLIN);
    agedBrieItemsUpdate(PRODUCT_POSITIVE_SELLIN);
    agedBrieItemsUpdate(PRODUCT_NEGATIVE_SELLIN_Q49);
    expect(PRODUCT_NEGATIVE_SELLIN.quality).to.be.below(51);
    expect(PRODUCT_POSITIVE_SELLIN.quality).to.be.below(51);
    expect(PRODUCT_NEGATIVE_SELLIN_Q49.quality).to.be.below(51);
  });

  it('The product sellin degrades just by one', () => {
    const PRODUCT_BACK_NEGATIVE_SELLIN: Item = { name: 'Backstage passes to a TAFKAL80ETC concert', sellIn: -2, quality: 49 };
    const PRODUCT_BACK_NEGATIVE_SELLIN_S1: Item = { name: 'Backstage passes to a TAFKAL80ETC concert', sellIn: 17, quality: 45 };
    const PRODUCT_BACK_NEUTRAL_SELLIN_Q49: Item = { name: 'Backstage passes to a TAFKAL80ETC concert', sellIn: 0, quality: 50 };
    backstagePassesItemsUpdate(PRODUCT_BACK_NEGATIVE_SELLIN);
    backstagePassesItemsUpdate(PRODUCT_BACK_NEGATIVE_SELLIN_S1);
    backstagePassesItemsUpdate(PRODUCT_BACK_NEUTRAL_SELLIN_Q49);
    expect(PRODUCT_BACK_NEGATIVE_SELLIN.sellIn).to.equal(-3);
    expect(PRODUCT_BACK_NEGATIVE_SELLIN_S1.sellIn).to.equal(16);
    expect(PRODUCT_BACK_NEUTRAL_SELLIN_Q49.sellIn).to.equal(-1);
  });

  it('Quality increases by one when sellin is greater than 10', () => {
    const PRODUCT_BACK: Item = { name: 'Backstage passes to a TAFKAL80ETC concert', sellIn: 11, quality: 8 };
    backstagePassesItemsUpdate(PRODUCT_BACK);
    expect(PRODUCT_BACK.quality).to.equal(9);
  });

  it('Quality increases by two when sellin is within 6 and 10', () => {
    const PRODUCT_BACK_10: Item = { name: 'Backstage passes to a TAFKAL80ETC concert', sellIn: 10, quality: 8 };
    const PRODUCT_BACK_6: Item = { name: 'Backstage passes to a TAFKAL80ETC concert', sellIn: 6, quality: 8 };
    backstagePassesItemsUpdate(PRODUCT_BACK_10);
    backstagePassesItemsUpdate(PRODUCT_BACK_6);
    expect(PRODUCT_BACK_10.quality).to.equal(10);
    expect(PRODUCT_BACK_6.quality).to.equal(10);
  });

  it('Quality increases by three when sellin is within 1 and 5', () => {
    const PRODUCT_BACK_10: Item = { name: 'Backstage passes to a TAFKAL80ETC concert', sellIn: 5, quality: 8 };
    const PRODUCT_BACK_6: Item = { name: 'Backstage passes to a TAFKAL80ETC concert', sellIn: 1, quality: 8 };
    backstagePassesItemsUpdate(PRODUCT_BACK_10);
    backstagePassesItemsUpdate(PRODUCT_BACK_6);
    expect(PRODUCT_BACK_10.quality).to.equal(11);
    expect(PRODUCT_BACK_6.quality).to.equal(11);
  });
});

describe('Testing the function conjuredItemsUpdate', () => {
  it('The function must return the object Item', () => {
    const PRODUCT_AGED: Item = { name: 'Aged Brie', sellIn: 30, quality: 80 };
    expect(conjuredItemsUpdate(PRODUCT_AGED)).to.be.an('object');
    expect(conjuredItemsUpdate(PRODUCT_AGED)).to.have.property('name');
    expect(conjuredItemsUpdate(PRODUCT_AGED)).to.have.property('sellIn');
    expect(conjuredItemsUpdate(PRODUCT_AGED)).to.have.property('quality');
  });

  it('The product quality needs to be equal or above 0', () => {
    const PRODUCT_NEGATIVE_SELLIN: Item = { name: 'Conjured', sellIn: -1, quality: 2 };
    const PRODUCT_POSITIVE_SELLIN: Item = { name: 'Conjured', sellIn: 2, quality: 3 };
    const PRODUCT_NEGATIVE_SELLIN_Q49: Item = { name: 'Conjured', sellIn: -10, quality: 1 };
    conjuredItemsUpdate(PRODUCT_NEGATIVE_SELLIN);
    conjuredItemsUpdate(PRODUCT_POSITIVE_SELLIN);
    conjuredItemsUpdate(PRODUCT_NEGATIVE_SELLIN_Q49);
    expect(PRODUCT_NEGATIVE_SELLIN.quality).to.be.above(-1);
    expect(PRODUCT_POSITIVE_SELLIN.quality).to.be.above(-1);
    expect(PRODUCT_NEGATIVE_SELLIN_Q49.quality).to.equal(0);
  });

  it('The product sellin degrades just by one', () => {
    const PRODUCT_CONJURED: Item = { name: 'Conjured', sellIn: -1, quality: 50 };
    const PRODUCT_CONJURED_PQ: Item = { name: 'Conjured', sellIn: 2, quality: 50 };
    const PRODUCT_CONJURED_NQ: Item = { name: 'Conjured', sellIn: -10, quality: 49 };
    conjuredItemsUpdate(PRODUCT_CONJURED);
    conjuredItemsUpdate(PRODUCT_CONJURED_PQ);
    conjuredItemsUpdate(PRODUCT_CONJURED_NQ);
    expect(PRODUCT_CONJURED.sellIn).to.equal(-2);
    expect(PRODUCT_CONJURED_PQ.sellIn).to.equal(1);
    expect(PRODUCT_CONJURED_NQ.sellIn).to.equal(-11);
  });

  it('Quality decreases by two when sellin decrease by one', () => {
    const PRODUCT_CONJURED: Item = { name: 'Conjured', sellIn: 1, quality: 32 };
    conjuredItemsUpdate(PRODUCT_CONJURED);
    expect(PRODUCT_CONJURED.quality).to.equal(30);
  });

  it('Quality decreases twice as fast when  the sell by date has passed', () => {
    const PRODUCT_CONJURED_NEGATIVE: Item = { name: 'Conjured', sellIn: -1, quality: 32 };
    const PRODUCT_CONJURED_ZERO: Item = { name: 'Conjured', sellIn: 0, quality: 32 };
    conjuredItemsUpdate(PRODUCT_CONJURED_NEGATIVE);
    conjuredItemsUpdate(PRODUCT_CONJURED_ZERO);
    expect(PRODUCT_CONJURED_NEGATIVE.quality).to.equal(28);
    expect(PRODUCT_CONJURED_ZERO.quality).to.equal(28);
  });
});

describe('Testing the function normalItemsUpdate', () => {
  it('The function must return the object Item', () => {
    const PRODUCT_AGED: Item = { name: 'Aged Brie', sellIn: 30, quality: 80 };
    expect(normalItemsUpdate(PRODUCT_AGED)).to.be.an('object');
    expect(normalItemsUpdate(PRODUCT_AGED)).to.have.property('name');
    expect(normalItemsUpdate(PRODUCT_AGED)).to.have.property('sellIn');
    expect(normalItemsUpdate(PRODUCT_AGED)).to.have.property('quality');
  });

  it('The product quality needs to be equal or less than 0', () => {
    const PRODUCT_NEGATIVE_SELLIN: Item = { name: 'Milk', sellIn: -1, quality: 2 };
    const PRODUCT_POSITIVE_SELLIN: Item = { name: 'Milk', sellIn: 2, quality: 3 };
    const PRODUCT_NEGATIVE_SELLIN_Q49: Item = { name: 'Milk', sellIn: -10, quality: 1 };
    normalItemsUpdate(PRODUCT_NEGATIVE_SELLIN);
    normalItemsUpdate(PRODUCT_POSITIVE_SELLIN);
    normalItemsUpdate(PRODUCT_NEGATIVE_SELLIN_Q49);
    expect(PRODUCT_NEGATIVE_SELLIN.quality).to.be.above(-1);
    expect(PRODUCT_POSITIVE_SELLIN.quality).to.be.above(-1);
    expect(PRODUCT_NEGATIVE_SELLIN_Q49.quality).to.equal(0);
  });

  it('The product sellin degrades just by one', () => {
    const PRODUCT: Item = { name: 'cereal', sellIn: -1, quality: 50 };
    const PRODUCT_PQ: Item = { name: 'cereal', sellIn: 2, quality: 50 };
    const PRODUCT_NQ: Item = { name: 'cereal', sellIn: -10, quality: 49 };
    normalItemsUpdate(PRODUCT);
    normalItemsUpdate(PRODUCT_PQ);
    normalItemsUpdate(PRODUCT_NQ);
    expect(PRODUCT.sellIn).to.equal(-2);
    expect(PRODUCT_PQ.sellIn).to.equal(1);
    expect(PRODUCT_NQ.sellIn).to.equal(-11);
  });

  it('Quality decreases by one when sellin decrease by one', () => {
    const PRODUCT: Item = { name: 'rice', sellIn: 1, quality: 32 };
    normalItemsUpdate(PRODUCT);
    expect(PRODUCT.quality).to.equal(31);
  });

  it('Quality decreases twice as fast when  the sell by date has passed', () => {
    const PRODUCT_NEGATIVE: Item = { name: 'car', sellIn: -1, quality: 32 };
    const PRODUCT_ZERO: Item = { name: 'car', sellIn: 0, quality: 32 };
    normalItemsUpdate(PRODUCT_NEGATIVE);
    normalItemsUpdate(PRODUCT_ZERO);
    expect(PRODUCT_NEGATIVE.quality).to.equal(30);
    expect(PRODUCT_ZERO.quality).to.equal(30);
  });
});
