/* eslint-disable linebreak-style */
/* eslint-disable no-param-reassign */
import { Item } from './gilded-rose';

export function verifyGeneralConditions(product: Item): Item {
  if (product.quality < 0 && product.name !== 'Sulfuras, Hand of Ragnaros') product.quality = 0;
  else if (product.name === 'Sulfuras, Hand of Ragnaros') product.quality = 80;
  else if (product.quality > 50) product.quality = 0;
  return product;
}

export function agedBrieItemsUpdate(product: Item): Item {
  if (product.quality < 50) {
    product.quality += 1;
    if (product.sellIn <= 0 && product.quality < 50) product.quality += 1;
  }
  product.sellIn -= 1;
  return product;
}

export function backstagePassesItemsUpdate(product: Item): Item {
  if (product.sellIn <= 0) product.quality = 0;
  else if (product.quality < 50) {
    product.quality += 1;
    if (product.sellIn < 11 && product.quality < 50) product.quality += 1;
    if (product.sellIn < 6 && product.quality < 50) product.quality += 1;
  }
  product.sellIn -= 1;
  return product;
}

export function conjuredItemsUpdate(product: Item): Item {
  if (product.sellIn <= 0) {
    if (product.quality <= 3) product.quality = 0;
    if (product.quality > 3) product.quality -= 4;
  } else if (product.quality > 1) product.quality -= 2;
  product.sellIn -= 1;
  return product;
}

export function normalItemsUpdate(product: Item): Item {
  if (product.quality > 0) {
    product.quality -= 1;
    if (product.sellIn <= 0 && product.quality > 0) product.quality -= 1;
  }
  product.sellIn -= 1;
  return product;
}
