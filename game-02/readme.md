# Game 02 - Gilded Rose

## Algorithm description
The algorithm updates the inventory for Gilded Rose, the previous method was a nightmare so i refactored it and now the updateQuality method is made up of microtasks, every microtask is a function and perform a little job, the functions is a good way to organize the code, the algorithm is founded in the single responsibility principle, which tells us that the functions should be small, should do only one thing and should do it well, it shouldn’t have nested structure or it should not have more than two indent level. 

For every category we have a function, so we have 5 functions and each updates a kind of item, this allow us to test the functionalities and have control about the method. The previous code can handle and do almost everything (perform multiple tasks) this was a big mistake, the algorithm also allows wrong values in the system, qualitys above 50 or less than 0 are examples, now all these mistakes were corrected.

Every function in conjunction with the test guarantee the following rules:

All the items except sulfuras must:
  - Has a quality value between 0 and 50
  - Sellin property degrades just by one
  
Sulfuras item must:
  - Has a quality value equal to 80
  - Sellin property never changes

Aged Brie items must:
  - Has a sellin property that degrades just by one
  - Has a quality that increases by one when sellin decrease by one 
  - Has a quality that twice as fast when  the sell by date has passed

Backstage Passes items must:
  - Has a quality value that drops to 0 after the concert
  - Has a quality that increases by one when sellin is greater than 10
  - Has a quality that increases by two when sellin is within 6 and 10
  - Has a quality that increases by three when sellin is within 1 and 5

Normal Items must:
  - Has a quality that decreases by one when sellin decrease by one
  - Has a quality that decreases by two when sellin is less or equal to 0

Conjured Items must:  **New Feature**
  - Has a quality that decreases by two when sellin decrease by one
  - Has a quality that decreases by four (twice as fast) when sellin is less or equal to 0

### How to use the new code?
You need to download all the modules so don't forget and put the next command in the terminal, the you can do the test for use the functionalities.

**npm install**

### Testing :clipboard:
I used mocha and chai to test, if you want to test all the functions just move to the game-02 folder and run this command in the terminal.

**npx mocha app/test/*.js**


Note: I didn´t alter the item class because i was afraid to touch the goblin's in the corner work, i know you would have covered me but is fine.