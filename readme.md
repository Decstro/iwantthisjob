# Let's start to job together

## I reached the goals

- I know that you love good code. Easy to read and with the best practices, me too. For this reason i used eslint, advices from clean code book and stackoverflow, i try to do a code with te best practices.

- You want a self-documenting code and i like this kind of code because the best documentation is a clean code. I used meaningful names for the functions and variables with the intention to make the code easy to understand, easy to change and easy to taken care of. I know that clean code should be simple and elegant.

- I think that the efficiency in execution time and memory usage can be the difference between a satisfied customer or the closure of a company. For this reason in the game 1 the solution code doesn't need to permute all the array to find the sum of a number, in the other hand in the game 2 the code follow the single responsibility principle (SRP), so i placed functions for do the job.

- The code is easy to be tested, in fact i used mocha and chai to test the functions because i want a good code, the tests help me to do a better job.

## Any doubts?
This is my email: juan.puello99@hotmail.com
My number: 3042134110

## Games

- [Game 01](https://gitlab.com/Decstro/iwantthisjob/-/tree/main/game-01)
- [Game 02](https://gitlab.com/Decstro/iwantthisjob/-/tree/main/game-02)

